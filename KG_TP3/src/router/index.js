import { createRouter, createWebHistory } from 'vue-router'
import IndexView from '../views/IndexView.vue'
import SignIn from '../views/SignIn.vue'
import SignUpView from '../views/SignUpView.vue'
import ProfileView from '../views/ProfileView.vue'
import AreaFormView from '../views/AreaFormView.vue'
import AreaView from '../views/AreaView.vue'
import AreasView from '../views/AreasView.vue'
import RouteFormView from '../views/RouteFormView.vue'
import RouteView from '../views/RouteView.vue'
import { jwtDecode } from "jwt-decode";
import { toast } from 'vue3-toastify';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Index',
      component: IndexView,
     
        
    },
    {
      path: '/signin',
      name: 'Signin',
      component: SignIn,
      meta: { requiresRetroaction: true }
    },
    {
      path: '/signup',
      name: 'Signup',
      component: SignUpView
    },
    {
      path: '/profile',
      name: 'Profile',
      component: ProfileView,
      // Page protégée, il faut être connecté pour y accéder
      meta: { requiresAuth: true,
              requiresRetroaction: true
            }

    },
    {
      path: '/areas/new',
      name: 'AreasNew',
      component: AreaFormView,
      // Page protégée, il faut être connecté pour y accéder
      meta: { requiresAuth: true }

    },
    {
      path: '/areas/:id/edit',
      name: 'AreasIdEdit',
      component: AreaFormView,
      // Page protégée, il faut être connecté pour y accéder
      meta: { requiresAuth: true }

    },
    {
      path: '/areas/:id',
      name: 'AreasId',
      component: AreaView
    },
    {
      path: '/areas',
      name: 'Areas',
      component: AreasView
    },
    {
      path: '/routes/new',
      name: 'RoutesNew',
      component: RouteFormView,
      // Page protégée, il faut être connecté pour y accéder
      meta: { requiresAuth: true }

    },
    {
      path: '/routes/:id/edit',
      name: 'RoutesIdEdit',
      component: RouteFormView,
      // Page protégée, il faut être connecté pour y accéder
      meta: { requiresAuth: true }

    },
    {
      path: '/routes/:id',
      name: 'RoutesId',
      component: RouteView
    },
    {
      path: '/403',
      name: 'AccessForbidden',
    },
    {
      path: '/404',
      name: 'NotFound',
    },
  ],
  linkActiveClass: 'active',
  linkExactActiveClass: 'exact',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }


})

// "Guarde de navigation", exécutée après chaque navigation  
router.afterEach((to, from, next) => {
  //rétroaction dynamique
  if(to.meta.requiresRetroaction){
    if(Object.keys(to.query).length && to.query.msg && to.query.msg != ""){
      //faire une rétro-action
      var notify = (function () {
        var executed = false
        return function() {
          if(!executed){
            executed = true
            let msg;
            if(to.query.msg == "user")
              msg = "Création de l'utilisateur!"
            else if(to.query.msg == "modif-route")
              msg = "Modification de la voie!"
            else if(to.query.msg == "creer-route")
              msg = "Création de la voie!"
            else if(to.query.msg == "modif-lieu")
              msg = "Modification du lieu!"
            else if(to.query.msg == "creer-lieu")
              msg = "Création du lieu!"

            if(msg != undefined){
              toast(msg, {
                autoClose: 5000,
                position: toast.POSITION.TOP_RIGHT,
             });
            }
            
          }}})()
          
            notify() 
        }} 
        next()
  })
    



// "Guarde de navigation", exécutée avant chaque navigation  
router.beforeEach((to, from, next) => {
  // Vérification de la présence d'un token
  // Mais on ne vérifie pas sa validité dans cet exemple
  // Pour une "vraie" application, il faudrait vérifier sa validité
  if (to.meta.requiresAuth) {
    if (localStorage.getItem('token')) {
      //valider le token
      let token = localStorage.getItem('token');
      // JWT à décoder
      // Décodage du JWT
      const decodedToken = jwtDecode(token);
      // Récupération des informations contenues dans le payload
     const dateNow = new Date();
      if (decodedToken.exp < (dateNow.getTime()/1000)) {
        localStorage.removeItem('token')
        next({ path: '/signin' });
      }
      next();
    } else {
      localStorage.removeItem('token')
      next({ path: '/signin' });
    }
  } else {
    next();
  }
});

// "Guarde de navigation", exécutée après chaque navigation



export default router


 // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      //component: () => import('../views/AboutView.vue')