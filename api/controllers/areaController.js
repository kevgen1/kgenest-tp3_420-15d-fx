"use strict";

const Area = require("../models/area");
const Route = require("../models/route");
const dotenv = require("dotenv");

dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

exports.createArea = async (req, res, next) => {
		   //destructurer le body.
           const { name, description, gettingThere, lon, lat, routes, user } = req.body;
            // Crée une nouvelle area
            const area = new Area({
               name: name,
               description: description,
               gettingThere: gettingThere,
               lon: lon,
               lat: lat,
               routes: routes,
               user: user,
             });
           try {
               // Enregistre la route dans la base de données
               const result = await area.save();
                   // Ajoute des liens pour les actions possibles sur la route
           const resultLinks = {
             ...result.toJSON(),
             links: [
                { rel: 'self', method:'POST', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/` },
                { rel: 'get', method:'PUT', href: `${url_base}/areas/${area._id}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${area._id}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/routes` },
               
             ]
           };
             
               res.location('/areas/' + result._id);
               return res.status(201).json(resultLinks);
       
           // TODO verifier comment retransmettre les champs en erreur, au formulaire.
             } catch (error) {
                // Gestion spécifique des erreurs de validation
               if (error.name === 'ValidationError') {
                 const errors = Object.values(error.errors).map(err => err.message);
                 console.log('Erreurs de validation :', errors);
                 // Renvoyer les erreurs au formulaire
                 const pageTitle = 'Ajouter une area';
                 return res.render('add_areas', { errors, name, description, gettingThere, lon, lat, routes, user, pageTitle });
               } else {
                 // Gestion des autres types d'erreurs
                 console.error('Erreur inattendue :', error);
                 next(error);
               }
           }
};


exports.getAreas = async (req, res, next) => {
	try {
        const areas = await Area.find().populate("routes");
        // Ajoute des liens pour les actions possibles sur chaque route
        const resultLinks = areas.map(area => {
          return {
            ...area.toJSON(),
            links: [
                { rel: 'post', method:'POST', href: `${url_base}/areas/` },
                { rel: 'self', method:'GET', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/` },
                { rel: 'get', method:'PUT', href: `${url_base}/areas/${area._id}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${area._id}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/routes` },
            ]
          };
        });
        res.status(200).json(resultLinks);
      } catch (err) {
        // Gestion des autres types d'erreurs
        next(err);
      }
  
};


exports.getUserAreas = async (req, res, next) => {
    try {
        const id = JSON.parse(req.user.data).id.toString()
        const areas = await Area.find({ user: id }).populate("routes").populate("user", "username");
        // Ajoute des liens pour les actions possibles sur chaque area
        const areaLinks = areas.map(area => {
          return {
            ...area.toJSON(),
            links: [
                { rel: 'post', method:'POST', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/` },
                { rel: 'self', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/` },
                { rel: 'get', method:'PUT', href: `${url_base}/areas/${area._id}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${area._id}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${area._id}/routes` },
            ]
          };
        });
        res.status(200).json(areaLinks);
      } catch (err) {
        next(err);
      }
};

exports.getArea = async (req, res, next) => {
    try {
        const areaId = req.params.areaId
        const result = await Area.findById( areaId ).populate({
          path : "routes",  //nom de la propriété dans l'objet parent
          model : Route,
          populate : {
              path : "area", //nom de la propriété dans l'objet parent
              model : Area,
          }
      });
        // Ajoute des liens pour les actions possibles sur chaque area
        const areaLinks = {
              ...result.toJSON(),
              links: [
                { rel: 'post', method:'POST', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'self', method:'GET', href: `${url_base}/areas/${result._id}/` },
                { rel: 'get', method:'PUT', href: `${url_base}/areas/${result._id}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${result._id}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${result._id}/routes` },
              ]
            };
        res.status(200).json(areaLinks);
      } catch (err) {
        next(err);
      }
    
};

exports.updateArea = async (req, res, next) =>{
    try {
        const areaId = req.params.areaId
        const result = await Area.findByIdAndUpdate( areaId, req.body, {new:true} );
        // Ajoute des liens pour les actions possibles sur chaque route
        const areaLinks = {
          ...result.toJSON(),
            links: [
                { rel: 'post', method:'POST', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${result._id}/` },
                { rel: 'self', method:'PUT', href: `${url_base}/areas/${result._id}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${result._id}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${result._id}/routes` },
            ]
          };
        res.status(200).json(areaLinks);
      } catch (err) {
        next(err);
      }
    
}

exports.deleteArea = async (req, res, next) =>{
    const areaId = req.params.areaId;
    try {
      await Area.findByIdAndRemove(areaId);
      const areaLinks = {
          links: [
            { rel: 'get', method:'GET', href: `${url_base}/areas/`},
          ]
        };
        //on retourne un 200. Parce qu'on veut retourner le link.
        res.status(200).send(areaLinks);
      } catch (err) {
      next(err);
    }

}

exports.getRoutes = async (req, res, next) => {
	const areaId = req.params.areaId;
    try {
        const result = await Area.findById(areaId).populate("routes");
         // Ajoute des liens pour les actions possibles sur chaque area
         const areaLinks = {
          ...result.toJSON(),
              links: [
                { rel: 'post', method:'POST', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/my-areas` },
                { rel: 'self', method:'GET', href: `${url_base}/areas/${areaId}/` },
                { rel: 'get', method:'PUT', href: `${url_base}/areas/${areaId}` },
                { rel: 'delete', method:'DELETE', href: `${url_base}/areas/${areaId}` },
                { rel: 'get', method:'GET', href: `${url_base}/areas/${areaId}/routes` },
              ]
            };
        //on retourne un 200. Parce qu'on veut retourner le link.
        res.status(200).send(areaLinks);
      } catch (err) {
      next(err);
    }
  
    
};


