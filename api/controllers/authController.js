"use strict";
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require("dotenv");
dotenv.config();
const url_base = process.env.URL + ":" + process.env.PORT;

const User = require("../models/user");

exports.logIn = async (req, res, next) => {
	const { email, password } = req.body;
  try {
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }

		// Vérifie si le mot de passe est valide
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      const error = new Error("Courriel ou mot de passe invalide");
      error.statusCode = 401;
      throw error;
    }
		// Création d'un jeton JWT
    const token = jwt.sign(
      {
        data: JSON.stringify({email: user.email, name: user.username, id: user.id})
        //email: user.email,
       // name: user.username,
       // id: user.id,
      },
      process.env.SECRET_JWT,
      { expiresIn: "24h" }
    );


    res.status(200).json({ token: token });
  } catch (err) {
    next(err);
  }

};

