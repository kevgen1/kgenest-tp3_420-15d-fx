"use strict";

const Route = require("../models/route");

const Area = require("../models/area");
const dotenv = require("dotenv");

dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;


exports.createRoute = async (req, res, next) => {
    //destructurer le body.
    const { name, type, grade, description, approach, descent, area, user } = req.body;
	 // Crée une nouvelle route

    const route = new Route({
        name: name,
        type: type,
        grade: grade,
        description: description,
        approach: approach,
        descent: descent,
        area: area,
        user: user,
      });
    try {
        // Enregistre la route dans la base de données
        let result = await route.save();
        await result.populate("area");
        //traitement des propriétés.
        const routeId = result._id.toString()
        const {routes} = result.area
        const nouvelle_route = [...routes, routeId]
        result.area.routes = nouvelle_route
        //une deuxième requête. Parce qu'il faut mettre à jour le champ routes de area.
        const area = await Area.findByIdAndUpdate( result.area._id, {routes: nouvelle_route}, {new:true} );
        //Je redonne l'area à la propriété area.
        result.area = area
        result = await result.save();
        await result.populate("area");
        // Ajoute des liens pour les actions possibles sur la route
    const resultLinks = {
      ...result.toJSON(),
      links: [
        { rel: 'self', method:'POST', href: `${url_base}/routes/` },
        { rel: 'get', method:'GET', href: `${url_base}/routes/my-routes` },
        { rel: 'get', method:'GET', href: `${url_base}/routes/` },
        { rel: 'get', method:'GET', href: `${url_base}/routes/${result._id}` },
        { rel: 'delete', method:'DELETE', href: `${url_base}/routes/${result._id}` },
        { rel: 'update', method:'PUT', href: `${url_base}/routes/${result._id}` },
        
      ]
    };

        res.location('/routes/' + result._id);
        return res.status(201).json(resultLinks);

      } catch (error) {
         // Gestion spécifique des erreurs de validation
        if (error.name === 'ValidationError') {
          const errors = Object.values(error.errors).map(err => err.message);
          console.log('Erreurs de validation :', errors);
    
          // Renvoyer les erreurs au formulaire
          const pageTitle = 'Ajouter une route';
          return res.render('add_route', { errors, name, type, grade, description, approach, descent, area, user, pageTitle });
        } else {
          // Gestion des autres types d'erreurs
          console.error('Erreur inattendue :', error);
          next(error);
        }
    }
  		
};


exports.getRoutes = async (req, res, next) => {
	
  try {
    // Get the current timestamp
    // on veut récupérer les req.query. S'il y en a.
      let query = {}
      let routes; 
      if(Object.keys(req.query).length !== 0){
        const {val_min, val_max, type, area} = req.query
        query = {
                  'grade.value': { $gte: val_min, $lte: val_max},
                  'type': type,
                  'area': area,
                }
        routes = await Route.find(query).populate("area");
      }
      else
          routes = await Route.find().populate("area");
      routes.sort((a, b) => {
        // Only sort on age if not identical //https://typeofnan.dev/sort-array-objects-multiple-properties-javascript/
        if (a.grade.value < b.grade.value) return -1;
        if (a.grade.value > b.grade.value) return 1;
        // Sort on name
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        // Both idential, return 0
        return 0;
      });

      const routeLinks = routes.map(route => {
        return {
          ...route.toJSON(),
          links: [
            { rel: 'self', method:'GET', href: `${url_base}/routes/` },
            { rel: 'post', method:'POST', href: `${url_base}/routes/`},
            { rel: 'get', method:'GET', href: `${url_base}/routes/my-routes`},
            { rel: 'get', method:'GET', href: `${url_base}/routes/${route._id}` },
            { rel: 'delete', method:'DELETE', href: `${url_base}/routes/${route._id}` },
            { rel: 'update', method:'PUT', href: `${url_base}/routes/${route._id}` },
          ]
        };
      });
      res.status(200).json(routeLinks);
      
    } catch (err) {
      next(err);
    }
};



exports.getUserRoutes = async (req, res, next) => {
    try {
      const id = JSON.parse(req.user.data).id.toString()
      const routes = await Route.find({ user: id }).populate("area").populate("user", "username");
      //Vouloir vérifier que ce sont les routes de l'utilisateur du token.
      
      
      // Ajoute des liens pour les actions possibles sur chaque route
      const routeLinks = routes.map(route => {
        return {
          ...route.toJSON(),
          links: [
            { rel: 'self', method:'GET', href: `${url_base}/routes/my-routes`},
            { rel: 'post', method:'POST', href: `${url_base}/routes/`},
            { rel: 'get', method:'GET', href: `${url_base}/routes/` },
            { rel: 'get', method:'GET', href: `${url_base}/routes/${route._id}` },
            { rel: 'delete', method:'DELETE', href: `${url_base}/routes/${route._id}` },
            { rel: 'update', method:'PUT', href: `${url_base}/routes/${route._id}` },
          ]
        };
      });
      res.status(200).json(routeLinks);
    } catch (err) {
      next(err);
    }
  
};

exports.getRoute = async (req, res, next) => {
  try {
    const routeId = req.params.routeId
    const result = await Route.findById( routeId ).populate("user", "username");
    await result.populate("area")

    // Ajoute des liens pour les actions possibles sur chaque route
    const resultLinks = {
      ...result.toJSON(),
      links: [
        { rel: 'self', method:'GET', href: `${url_base}/routes/${result._id}` },
        { rel: 'post', method:'POST', href: `${url_base}/routes/`},
        { rel: 'get', method:'GET', href: `${url_base}/routes/my-routes`}, 
        { rel: 'get', method:'GET', href: `${url_base}/routes/` },
        { rel: 'delete', method:'DELETE', href: `${url_base}/routes/${result._id}` },
        { rel: 'update', method:'PUT', href: `${url_base}/routes/${result._id}` },
        
      ]
    };
    res.status(200).json(resultLinks);
  } catch (err) {
    next(err);
  }
    
};

exports.updateRoute = async (req, res, next) =>{
  
  try {
    const routeId = req.params.routeId
    const result = await Route.findByIdAndUpdate( routeId, req.body, {new:true} );
    // Ajoute des liens pour les actions possibles sur chaque route
    const resultLinks = {
      ...result.toJSON(),
        links: [
          { rel: 'self', method:'PUT', href: `${url_base}/routes/${result._id}` },
          { rel: 'post', method:'POST', href: `${url_base}/routes/`},
          { rel: 'get', method:'GET', href: `${url_base}/routes/${result._id}` },
          { rel: 'get', method:'GET', href: `${url_base}/routes/my-routes`}, 
          { rel: 'get', method:'GET', href: `${url_base}/routes/` },
          { rel: 'delete', method:'DELETE', href: `${url_base}/routes/${result._id}` },
        ]
      };
    res.status(200).json(resultLinks);
  } catch (err) {
    next(err);
  }


}

exports.deleteRoute = async (req, res, next) =>{
  console.log("##################################### effacer")
  const routeId = req.params.routeId;
  try {
    await Route.findByIdAndRemove(routeId);
    const routeLinks = {
        links: [
          { rel: 'get', method:'GET', href: `${url_base}/routes/`},
        ]
      };
      //on retourne un 200. Parce qu'on veut retourner le link.
      res.status(200).send(routeLinks);
    } catch (err) {
    next(err);
  }


}
