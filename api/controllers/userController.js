"use strict";

const User = require("../models/user");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");

dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

exports.createUser = async (req, res, next) => {

    try {
		const { username, email, password} = req.body;
		console.log("###################################################", req.body)
		console.log("username not defined", username)
	 	// Crée un nouveau user
		// Encryption du mot de passe
		const hashedPassword = await bcrypt.hash(password, 10);
		// Création d'un nouvel utilisateur
    	const user = new User({
      		email: email,
      		username: username,
      		password: hashedPassword,
    	});
		 // Enregistre la route dans la base de données
			const result = await user.save();
        // Ajoute des liens pour les actions possibles sur la route
    	const resultLinks = {
    	  ...result.toJSON(),
    	  links: [
    	    { rel: 'self', method:'POST', href: `${url_base}/users/`},
			{ rel: 'get', method:'GET', href: `${url_base}/users/${result._id}`},
    	  ]
    	};
        res.location('/users/' + result._id);
        return res.status(201).json(resultLinks);
      } catch (err) {
		console.log("catch erreur 500", err)
		next(err);
	  }
};


exports.getUser = async (req, res, next) => {
	try {
		const idUtilisateur = req.params.utilisateurId
		const result = await User.findById( idUtilisateur );
		if (!result) {
			const error = new Error('L\'user n\'existe pas.');
			error.statusCode = 404;
			throw error;
		}
		// Ajoute des liens pour les actions possibles sur chaque route
		const resultLinks = {
		  ...result.toJSON(),
			links: [
			  { rel: 'post', method:'POST', href: `${url_base}/users/`},
			  { rel: 'self', method:'GET', href: `${url_base}/users/${result._id}`},
			]
		  };
		res.status(200).json(resultLinks);
	  } catch (err) {
		next(err);
	  }
};






