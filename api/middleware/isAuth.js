const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();


	/** Vérifie si la requête a un token JWT valide */

module.exports = (req, res, next) => {
  
	// Récupère le jeton depuis l'en-tête Authorization de la requête
  const authHeader = req.get('Authorization');
	// Vérifie si l'en-tête Authorization est présent
  if (!authHeader) {
    return res.status(401).json({ error: 'Non authentifié.' });
  }

	// Récupère le jeton JWT
  const token = authHeader.split(' ')[1];
  let decodedToken;

  try {
	  // Vérifie le jeton et récupére les données associées
    decodedToken = jwt.verify(token, process.env.SECRET_JWT);
		// Ajoute les données associées à l'objet de requête pour utilisation ultérieur
    req.user = decodedToken 
    //si le token est expiré. 
    const dateNow = new Date();
    if (decodedToken.exp < (dateNow.getTime()/1000)) {
      const error = new Error('Le token est expiré');
      error.statusCode = 401;
      throw error;
    } 
    const obj_data = JSON.parse(req.user.data);
    const user_id = obj_data.id
    req.user.user_id = user_id
    next();
  } catch (err) {

    err.statusCode = 401;
    return next(err);
  }
};
