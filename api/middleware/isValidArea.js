
/** Vérifie si la requête est valide. Et, si les champs sont valides. */

module.exports = (req, res, next) => {

   try {
         // Ajoute les données associées à l'objet de requête pour utilisation ultérieure
     //Verifier pour bad request
     const { name, description, gettingThere, lon, lat, routes, user } = req.body;
     if(!name || !description  || !gettingThere  || (lon != 0 && lon == undefined) || (lat != 0 && lat == undefined) || !user ){
        const error = new Error(`Un des paramètres de la requête est invalide ou manquant.`);
        error.statusCode = 400;
        throw error;
      }
     if(name.length == 0){ //Au moins 1 caractère Requis
         const error = new Error('La longueur de name est inférieure à la contrainte 1.');
         error.statusCode = 422;
         throw error;
     }
     else if(description.length == 0){ //Requis
         const error = new Error('La description ne correspond pas aux choix permis.');
         error.statusCode = 422;
         throw error;
     }
     else if(gettingThere.length == 0){ //Requis
         const error = new Error('La gettingThere de area est inférieure à la contrainte 1.');
         error.statusCode = 422;
         throw error;
     }
     else if(lon < -180 && lon > 180){ //Entre -180 et 180 Requis
         const error = new Error('La longitude n\'est pas entre -180 et 180.');
         error.statusCode = 422;
         throw error;
     }
     else if(lat < -90 && lat > 90){ //Entre -90 et 90 Requis
        const error = new Error('La longitude n\'est pas entre -90 et 90.');
        error.statusCode = 422;
        throw error;
    }
   
     next();
   } catch (err) {
     return next(err);
   }
 };
 