
/** Vérifie si le nouveau name de la route existe.*/
const Route = require("../models/route");

module.exports = async (req, res, next) => {

try {
    const {name} = req.body
    //Vérifier si l'user existe.
    const result = await Route.findOne({name: name});
    if(result) {
        if(result.name === name){
          if(req.params.id){ //Modification
            if( result._id != req.params.id){
              const error = new Error(`Le nom n'est pas disponible. *name: ${name}`);
              error.statusCode = 409;
              throw error;
            }
          }
          else{ //Création
            const error = new Error(`Le nom n'est pas disponible. *name: ${name}`);
            error.statusCode = 409;
            throw error;
          } 
        }
    }
    next()
      } catch (err) {
        console.log("catch erreur", err)		
        next(err);
      }
    }