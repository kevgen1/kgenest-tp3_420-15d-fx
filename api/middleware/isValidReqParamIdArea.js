const Area = require("../models/area");



module.exports = async (req, res, next) => {
    try {
            //Vérifier si l'area existe.
         const areaId = req.params.areaId
         const area = await Area.findById(areaId);
         if (!area) {
             const error = new Error('L\'area n\'existe pas.');
             error.statusCode = 409;
             throw error;
         }
        next()
      } catch (err) {
        next(err);
      }
}