

const Route = require("../models/route");
const User = require("../models/user");



module.exports = async (req, res, next) => {
    try {
      //Vérifier si la route existe.
        const routeId = req.params.routeId
        if (!routeId) {
          const error = new Error('Le paramètre de la route est manquant.');
          error.statusCode = 422;
          throw error;
        }
        else if(routeId){
            const regex = /^[0-9a-fA-F]{24}$/;
            const resultat = regex.test(routeId)
            if(!resultat){
                const error = new Error('Le paramètre de requête routeId est invalide.');
                error.statusCode = 422;  
                throw error; 
            }
        }
        const route = await Route.findById(routeId);
        if (!route) {
            const error = new Error('La route n\'existe pas.');
            error.statusCode = 404;
            throw error;
        }
        //ajouter à l'objet request la valeur obtenue de la bd.
        req.routeUser = route.user
        next()
      } catch (err) {
        next(err);
      }
}