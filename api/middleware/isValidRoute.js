
/** Vérifie si la requête est valide. Et, si les champs sont valides. */

module.exports = (req, res, next) => {

  try {
		// Ajoute les données associées à l'objet de requête pour utilisation ultérieure
    //le champ nom Au moins 1 caractère Requis
    const { name, type, grade, description, approach, descent, area, user } = req.body;
    const req_body = {name, type, grade, description, approach, descent, area, user}
    let bool_flag = []
    if(!name || !type  || !grade  || (grade && !grade.text)  || (grade && !grade.value) || !description  || !area || !user){
        Object.keys(req_body).forEach(function eachKey(key) {
            
            let error;
            if(!req_body[key]){
                bool_flag.push(`${key}:${req_body[key]}`)
                if (typeof req_body[key] === "string" && req_body[key].length === 0){
                    error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                    error.statusCode = 422;
                }
                else if(req_body[key] === null || req_body[key] === undefined ){
                    error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                    error.statusCode = 400;
                }
                throw error;
            }
            else if(Object.prototype.toString.apply(req_body[key]) ==='[object Object]'){
                if(req_body[key].value === "" || req_body[key].text === ""){
                    if(req_body[key].value === ""){
                        bool_flag.push(`${key}:{${req_body[key]}: ${req_body[key].value}}`)
                        error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                        error.statusCode = 422;
                    }
                    else{
                        bool_flag.push(`${key}:{${req_body[key]}: ${req_body[key].text}}`)
                        error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                        error.statusCode = 422;
                    }  
                    throw error;      
                }
                else if(req_body[key].value === null || req_body[key].value === undefined || req_body[key].text === null || req_body[key].text === undefined){
                    if(req_body[key].value === null || req_body[key].value === undefined){
                        bool_flag.push(`${key}:{${req_body[key]}: ${req_body[key].value}}`)
                        error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                        error.statusCode = 400;
                    }
                    else{
                        bool_flag.push(`${key}:{${req_body[key]}: ${req_body[key].text}}`)
                        error = new Error(`Un des paramètres de la requête est invalide ou manquant. Paramètre(s): ${bool_flag.join(",")}`);
                        error.statusCode = 400;
                    }
                    throw error;        
                }
            }
    });

    }
    else if(type != "SPORT" && type != "TR" && type != "TOPROPE"){
        const error = new Error('Le type ne correspond pas aux choix permis.');
        error.statusCode = 422;
        throw error;
    }
    else if(area.length !== 0){
        const regex = /^[0-9a-fA-F]{24}$/;
        const resultat = regex.test(req_body["area"])
        if(!resultat){
            const error = new Error('La longueur de area est inférieure à la contrainte 1.');
            error.statusCode = 422;  
            throw error; 
        }
    }
   
    next();
  } catch (err) {
    return next(err);
  }
};



