
/** Vérifie si la requête est valide. Et, si les champs sont valides. */
const User = require("../models/user");

module.exports = async (req, res, next)=>{
	const {username, email, password, confirmedPassword} = req.body
	//champs requis
	
	if (!username || username.trim() == "" || !email || email.trim() == "" || !password || password.trim() == "" || !confirmedPassword || confirmedPassword.trim() == "") {
		const err = new Error("Tous les champs sont requis");
		err.statusCode = 400;
		return next(err);
	}
	//username
	if (!(username.length >= 3 && username.length <= 50)) {
		const err = new Error("Entre 3 et 50 caractères.");
		err.statusCode = 422;
		return next(err);
	}

	//email
	const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	if(!emailRegex.test(email)){
		const err = new Error("Le format du courriel est invalide");
		err.statusCode = 422;
		return next(err);
	}

	//longueur du email
	if(email.length > 50){
		const err = new Error("Pas plus de 50 caractères.");
		err.statusCode = 422;
		return next(err);
	}

	//mot de passe
	if(password.length < 6){
		const err = new Error("Le mot de passe doit contenir au moins 6 caracères" );
		err.statusCode = 422;
		return next(err);
	}
	
	if (password !== confirmedPassword) {
		const err = new Error("Les mots de passes ne correspondent pas" );
		err.statusCode = 409;
		return next(err);
	}
	let message = "L\'user\'existe déjà."
	let result_email;
	try {
		//Vérifier si l'email existe, pour un utilisateur.
		result_email = await User.findOne({ email: email });
		if(result_email) 
			message +=`*email: ${email}`
  		} catch (err) {
			console.log("catch erreur", err)		
			next(err);
  	}
	let result_username;
	try {
		//Vérifier si l'username existe, pour un utilisateur.
		result_username = await User.findOne({ username: username });
		if(result_username) 
			message +=`*username: ${username}`
  		} catch (err) {
			console.log("catch erreur", err)		
			next(err);
  	}
	try {
		if(result_email || result_username){
			const error = new Error(message);
			error.statusCode = 409;
			throw error;
		}
	}
	catch (err) {
		console.log("catch erreur", err)		
		next(err);
	}
	
	next()
};
