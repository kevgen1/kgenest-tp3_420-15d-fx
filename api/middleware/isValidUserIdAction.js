
/** Vérifier si l'utilisateur peut agir sur la route. */
module.exports = (req, res, next)=>{
    try{
        const obj_data = JSON.parse(req.user.data);
        const user_id = obj_data.id
        if(req.routeUser !== user_id){
            const error = new Error('Action interdite sur cette route.');
			error.statusCode = 403;
			throw error;
        }
        next()
    }
  	catch (err) {
			next(err);
  		}
}