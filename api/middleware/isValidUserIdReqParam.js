/** Vérifier si l'utilisateur peut agir sur la route. */
const User = require("../models/user");
module.exports = async (req, res, next)=>{
    try{
        const user_id = req.params.utilisateurId
        const id_regex = /^[0-9a-fA-F]{24}$/  
        if(!user_id){
            const error = new Error('Paramètre de requête inexistant.');
			error.statusCode = 400;
			throw error;
        }
        if(!id_regex.test(user_id)){
            const error = new Error('Paramètre de requête non-conforme.');
			error.statusCode = 422;
			throw error;
        }
        //Vérifier si l'user existe.
		const result = await User.findById(user_id);
		if (!result) {
			const error = new Error('L\'user n\'existe pas.');
			error.statusCode = 404;
			throw error;
		}
		next()
  		} catch (err) {
			next(err);
  		}    
}
