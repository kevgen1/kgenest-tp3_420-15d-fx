"use strict";

const express = require("express");
const router = express.Router();
const areaController = require("../controllers/areaController");
//importer le middleware
const isAuth = require("../middleware/isAuth");
const isValidArea = require("../middleware/isValidArea");
const isValidReqParamIdArea = require("../middleware/isValidReqParamIdArea");
const isValidNameArea = require("../middleware/isValidNameArea");
const isValidUserIdAction = require("../middleware/isValidUserIdAction");
//On veut être certain de cors
const cors = require("../middleware/cors");

// /areas/ => POST
router.post("/areas/", isAuth, isValidArea, isValidNameArea, areaController.createArea);

// /areas/ => GET
router.get("/areas/", areaController.getAreas);

// /areas/my-areas => GET
router.get("/areas/my-areas", isAuth, areaController.getUserAreas);

// /areas/:areaId => GET
router.get("/areas/:areaId", isValidReqParamIdArea, areaController.getArea);

// /areas/:areaId/routes => GET
router.get("/areas/:areaId/routes", isValidReqParamIdArea, areaController.getRoutes);

// /areas/:areaId => PUT
router.put("/areas/:areaId", isAuth, isValidUserIdAction, isValidReqParamIdArea, isValidArea, isValidNameArea, areaController.updateArea);

// /areas/:areaId => DELETE
router.delete("/areas/:areaId", isAuth, isValidUserIdAction, isValidReqParamIdArea, areaController.deleteArea);

module.exports = router;

