"use strict";

const express = require("express");

const authController = require("../controllers/authController");
//On veut être certain de cors
const cors = require("../middleware/cors");

const router = express.Router();

// login/ => POST
router.post("/signIn", authController.logIn);

module.exports = router;
