"use strict";

const express = require("express");
const seedController = require("../controllers/dbController");
//On veut être certain de cors
const cors = require("../middleware/cors");
const router = express.Router();

router.get("/db/seed", seedController.seed);

module.exports = router;