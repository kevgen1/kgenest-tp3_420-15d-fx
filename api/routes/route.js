"use strict";

const express = require("express");

const router = express.Router();

const routeController = require("../controllers/routeController");
//importer le middleware
const isAuth = require("../middleware/isAuth");
const isValidRoute = require("../middleware/isValidRoute");
const isValidReqParamIdRoute = require("../middleware/isValidReqParamIdRoute");
const isValidUserIdAction = require("../middleware/isValidUserIdAction");
const isValidNameRoute = require("../middleware/isValidNameRoute");

//On veut être certain de cors
const cors = require("../middleware/cors");

// /routes/ => POST
router.post("/routes/", isAuth, isValidRoute, isValidNameRoute, routeController.createRoute); //accédera au middleware pour vérifier le token

// /routes/ => GET
router.get("/routes/", routeController.getRoutes);

// /routes/my-routes => GET
router.get("/routes/my-routes", isAuth, routeController.getUserRoutes);

// /routes/:routeId => GET
router.get("/routes/:routeId", isValidReqParamIdRoute, routeController.getRoute);

// /routes/:routeId => PUT
router.put("/routes/:routeId", isAuth, isValidRoute, isValidReqParamIdRoute, isValidNameRoute, isValidUserIdAction, routeController.updateRoute);

// /routes/:routeId => DELETE
router.delete("/routes/:routeId", isAuth, isValidReqParamIdRoute, isValidUserIdAction, routeController.deleteRoute);


module.exports = router;

