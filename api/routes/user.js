"use strict";

const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");
const isValidUser = require("../middleware/isValidUser");
const isValidUserIdReqParam = require("../middleware/isValidUserIdReqParam");
//On veut être certain de cors
const cors = require("../middleware/cors");

// /users/ => POST
router.post("/users/", isValidUser, userController.createUser);
//router.post("/users/",  userController.createUser);

// /users/:utilisateurId => GET (utilisateur connecté)
router.get("/users/:utilisateurId", isValidUserIdReqParam, userController.getUser);


module.exports = router;

