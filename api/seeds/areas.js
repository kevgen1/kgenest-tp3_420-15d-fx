//coordonnees de new york
const areas = [ 
    {
      "_id": "620f5b8b8b8b8b8b8b8b8b8d",
      "name": "Northern Box areas Climbing",
      "description": `The northern Box areas are home to a variety of good climbing and bouldering. Overhanging juggy bouldering along the streambed near US60, and the surrounding canyon walls offer other good climbing. Please see map layout of different climbing walls and bouldering areas.`,
      "gettingThere": `All the areas here can be accessed from the main Box parking lot. About 1/4 mile after turning on the dirt from US60, veer left at the Y and the this parking lot is to the left before the road crosses the wash. There is a trash receptacle, toilet, and information kiosk here.`,
      "lon": "-106.9905",
      "lat": "34.0033",
      "routes": ["630f5b8b8b8b8b8b8b8b8b8a", "630f5b8b8b8b8b8b8b8b8b8b", "630f5b8b8b8b8b8b8b8b8b8c", "630f5b8b8b8b8b8b8b8b8b8d", "630f5b8b8b8b8b8b8b8b8b8e"], //vecteur id
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
    },//https://www.mountainproject.com/area/105791881/northern-box-areas
    {
      "_id": "620f5b8b8b8b8b8b8b8b8b8e",
      "name": "Main Climbing Areas",
      "description": `These are the areas accessible from Linwood Road and where the vast majority of climbs are located. These areas include the Practice Wall, David's Castle, Red Wall, and the Fortress area.`,
      "gettingThere": `From I-85, take Exit 13 and go Left off the ramp. Continue through the light and the road will become Archie Whitesides Rd. (SR 1122). Follow this for roughly 2 miles until it dead-ends into Linwood Rd. Go Left on Linwood Rd. for about 0.1 mile and make a Right into the parking lot. Here you'll find an information kiosk and restrooms.`,
      "lon": "-81.27453",
      "lat": "35.23318",
      "routes": ["630f5b8b8b8b8b8b8b8b8b8f", "630f5b8b8b8b8b8b8b8b8b9a", "630f5b8b8b8b8b8b8b8b8b9b", "630f5b8b8b8b8b8b8b8b8b9c", "630f5b8b8b8b8b8b8b8b8b9d"], //vecteur id
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
    }, //https://www.mountainproject.com/area/106099520/main-climbing-areas
    {
      "_id": "620f5b8b8b8b8b8b8b8b8b8f",
      "name": "Central-West Cascades & Seattle Climbing",
      "description": `The area from Seattle-Puget Sound to Snoqualmie Pass and Steven's Pass, covering King County and the southern half of Snohomish County.`,
      "gettingThere": `Mainly I5 N-S and either I90 or HW2 E-W.`,
      "lon": "-121.51181",
      "lat": "47.75954",
      "routes": ["630f5b8b8b8b8b8b8b8b8b9e", "630f5b8b8b8b8b8b8b8b8b9f", "630f5b8b8b8b8b8b8b8b8b0a", "630f5b8b8b8b8b8b8b8b8b0b", "630f5b8b8b8b8b8b8b8b8b0c"], //vecteur id
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
    }, //https://www.mountainproject.com/area/108471374/central-west-cascades-seattle
    {
      "_id": "620f5b8b8b8b8b8b8b8b8b81",
      "name": "*Phoenix Areas Climbing",
      "description": `Phoenix is a winter mecca and summertime is long. This large desert city is home to a substantial amount of classic climbing. Whether you are trying to escape winter, or call it home, you have plenty of options to explore some stone.`,
      "gettingThere": `From Phoenix you can drive in town to your favorite afternoon bouldering area or head to North Scottsdale for great granite traditional climbing.`,
      "lon": "-112.07699",
      "lat": "33.5046",
      "routes": ["630f5b8b8b8b8b8b8b8b8b0d", "630f5b8b8b8b8b8b8b8b8b0e", "630f5b8b8b8b8b8b8b8b8b0f", "630f5b8b8b8b8b8b8b8b8b1a", "630f5b8b8b8b8b8b8b8b8b1b"], //Diamond View Boulder Climbing, Lone Star Bouldering, 
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
    }, //https://www.mountainproject.com/area/107448852/phoenix-areas
  ]
  
  
  
  module.exports = areas;