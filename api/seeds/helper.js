const bcrypt = require('bcrypt');
const motDePasse = '123qwe';
const saltRounds = 10; //Nombre de tours de chiffrement
// Hachage du mot de passe avec une promesse
bcrypt.hash(motDePasse, saltRounds)
  .then(hash => {
    // Affichage du mot de passe haché
    console.log('Mot de passe haché :', hash)
  })
  .catch(err => {
    console.error(err);
  });