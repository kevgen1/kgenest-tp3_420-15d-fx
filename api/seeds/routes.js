const routes =
[
  { //area: 620f5b8b8b8b8b8b8b8b8b8d "630f5b8b8b8b8b8b8b8b8b8a", "630f5b8b8b8b8b8b8b8b8b8b", "630f5b8b8b8b8b8b8b8b8b8c", "630f5b8b8b8b8b8b8b8b8b8d", "630f5b8b8b8b8b8b8b8b8b8d"
      "_id": "630f5b8b8b8b8b8b8b8b8b8a", //Trad  ["SPORT", "TRAD", "TOPROPE"]
      "name": "Red Wall",
      "type": "TRAD", 
      "grade": {
                "text" : "5.11a",
                "value" : 5.111,
              },
      "description": `Red Wall is the tallest cliff at Socorro Box, about 170' tall. It faces southwest and gets sun from late morning to late afternoon. About 15 climbs are on Red Wall- some are decent moderate sport climbs, and some trad climbs. Many of these stop about halfway up, but a few lines continue all the way up. In general, the 2nd pitches (other than "Big Red Roof") are not worth it; the rock is less good and the climbing easier... but it is worth going to the top once for the experience. The Corner Block, at the left end of Red Wall, hosts 5 more routes too. Despite its height and fun climbing, Red Wall is less popular than some of the other cliffs nearby, perhaps because of incomplete info and that many older climbs require gear to supplement bolts for protection.`,
      "approach": "Locate a large right facing dihedral which starts about 30 feet up from the ground.",
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required.",
      "area": "620f5b8b8b8b8b8b8b8b8b8d",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106642752/red-wall
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b8b", //Trad 
      "name": "Spiderman",
      "type": "TRAD",
      "grade": {
                "text" : 5.8,
                "value" : 5.08,
              },
      "description": `Gave this a "Great" rating just for P1. P2 is okay. Can rap from the top of P1 with a 60 meter rope. Don't let the 5.9 rating scare you away as it applies to the P1 variation and is well protected.`,
      "approach": `We started roughly 50 feet uphill from the direct start to Red Wall route, beneath four safely spaced bolts (P1 Var).`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required.",
      "area": "620f5b8b8b8b8b8b8b8b8b8d",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106642738/spiderman
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b8c", //TR 
      "name": "Little Red Wall",
      "type": "TOPROPE",
      "grade": {
                "text" : "5.10d",
                "value" : 5.104,
              },
      "description": `The red face just right of The Chimney. This is a fun thin face problem. Very small edges, maybe painful, and sustained, especially at the thin crux 10' before the top. Stay true to the line and don't use holds for the Chimney to the left, or those for the shallow corner system to the right (Black Hole) to keep it at the 5.11 grade.`,
      "approach": "The obvious chimney glaring you in the face.",
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Toprope from a bolted anchor at the top of the cliff (the anchor is set back 2', so you'll need 4 shoulder length runners or so). This climb had bolts at one point, but none are currently present.",
      "area": "620f5b8b8b8b8b8b8b8b8b8d",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106359260/little-red-wall
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b8d", //TR 
      "name": "Waterfall",
      "type": "TOPROPE",
      "grade": {
                "text" : 5.9,
                "value" : 5.09,
              },
      "description": `Somewhat blocky climb on typical Socorro rhyolite. Some old bolts without hangers or cutoff.  Looked like opportunities for trad protection placement very limited.`,
      "approach": `On the left-mid side of the black streaked waterfall. Immediately left of Beginners Route.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Two-bolt anchor at top without chains or lowering carabiners.",
      "area": "620f5b8b8b8b8b8b8b8b8b8d",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/118323364/waterfall
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b8e", //Sport 
      "name": "Wood",
      "type": "SPORT",
      "grade": {
                "text" : "5.13c",
                "value" : 5.133,
              },
      "description": `Another good beginner lead with fun moves and bolts where you want them. An easy, low-angle start leads to steeper moves with big holds through a 5.7/5.8 crux.`,
      "approach": `The 3rd route from the right on Hueco Wall (the cliff right of Dirt Wall).`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. 5 bolts to 2 bolt anchor with steel lowering carabiners.",
      "area": "620f5b8b8b8b8b8b8b8b8b8d",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106096092/wood
    
    { //area: 630f5b8b8b8b8b8b8b8b8b8e "630f5b8b8b8b8b8b8b8b8b8e", "630f5b8b8b8b8b8b8b8b8b8f", "630f5b8b8b8b8b8b8b8b8b8f", "630f5b8b8b8b8b8b8b8b8b81", "630f5b8b8b8b8b8b8b8b8b82"
      "_id": "630f5b8b8b8b8b8b8b8b8b8f", //Trad 
      "name": "Cro-Magnon Crack",
      "type": "TRAD",
      "grade": {
                "text" : 5.8,
                "value" : 5.08,
              },
      "description": `Climb the fun, too short left angling crack to the top. Named because it is a step above the cave, man. There is an old mining tunnel (cave)down and right of this wall. Well worth a visit, and spooky, even if it does smell like pee.`,
      "approach": `At left end of Rawlhide Wall, which is below Plane Above Your Head.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Trad rack.",
      "area": "620f5b8b8b8b8b8b8b8b8b8e",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106246786/cro-magnon-crack
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b9a", //Trad 
      "name": "Ditch Two Pitch",
      "type": "TRAD",
      "grade": {
                "text" : 5.8,
                "value" : 5.08,
              },
      "description": `P1 - Climb the first pitch of Two Pitch(5.4) and set up a natural anchor in the 'triangle' near the big ledge. P2 - Traverse left towards the second pitch of Two Pitch(5.4) but instead of taking the 'ramp', stem out right and climb straight up some exposed heady but physically easy terrain.`,
      "approach": `Cliff right of Two Pitch (5.4).`,
      "descent": `Trad Rack - Micros-Medium (#000-#2) double up on some of the micros(#0-00) and mediums (#.5-#.75). Natural Anchor (#.5-#.75-#1).`,
      "area": "620f5b8b8b8b8b8b8b8b8b8e",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/107626771/ditch-two-pitch
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b9b", //Sport 
      "name": "New Policy",
      "type": "SPORT",
      "grade": {
                "text" : "5.10d",
                "value" : 5.104,
              },
      "description": `At right end of New Policy Wall, left of a rotten fissure. Climb the face passed 3 bolts to the top. Fixed anchors.`,
      "approach": `At right side of wall, left of rotten fissure.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. 3 bolts and NC trad rack.",
      "area": "620f5b8b8b8b8b8b8b8b8b8e",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106245861/new-policy
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b9c", //Sport 
      "name": "Energy Czar",
      "type": "SPORT",
      "grade": {
                "text" : "5.10d",
                "value" : 5.104,
              },
      "description": `Start right of the large obvious chimney.Climb the steep corner to an overhang. Continue up the face to anchors at a ledge.`,
      "approach": `5 bolts. #2friend can be placed mid-crux.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required.",
      "area": "620f5b8b8b8b8b8b8b8b8b8e",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106427936/energy-czar
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b9d", //TR 
      "name": "Black Flag",
      "type": "TOPROPE",
      "grade": {
                "text" : "5.10",
                "value" : 5.100,
              },
      "description": `Start on the arete and face just left of Gastonia Crack. Traverse across the horizontal with an old pin in it. Hard moves to a bolt at a 'stance'. Rest up here and climb through the roof to the top anchors.`,
      "approach": `Usually toproped.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required.",
      "area": "620f5b8b8b8b8b8b8b8b8b8e",
      "user": "634f5b8b8b8b8b8b8b8b8b1a",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106658106/black-flag

    { //area: 630f5b8b8b8b8b8b8b8b8b8f "630f5b8b8b8b8b8b8b8b8b83", "630f5b8b8b8b8b8b8b8b8b84", "630f5b8b8b8b8b8b8b8b8b85", "630f5b8b8b8b8b8b8b8b8b86", "630f5b8b8b8b8b8b8b8b8b82"
      "_id": "630f5b8b8b8b8b8b8b8b8b9e", //Trad 
      "name": "Godzilla",
      "type": "TRAD",
      "grade": {
                "text" : "5.9",
                "value" : 5.09,
              },
      "description": `Another Index classic. Godzilla is the crack/corner system immediately right of City Park (5.13c), a fairly obvious landmark. P1: (5.9) Start in a shallow right-facing groove/face, just right of being directly below the obvious City Park crack. Climb up and trend left to the big flake at the start of the main crack/corner system. Continue up the obvious right-facing corner-crack system and then traverse left along some ledges up to a bolted belay/rap station.`,
      "approach": `Godzilla is the crack/corner system immediately right of City Park.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Standard trad rack to 3", bolted belay/rap station.`,
      "area": "620f5b8b8b8b8b8b8b8b8b8f",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105790717/godzilla
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b9f", //Trad 
      "name": "Pisces",
      "type": "TRAD",
      "grade": {
                "text" : 5.8,
                "value" : 5.08,
              },
      "description": `This perfect splitter handcrack is really a continuation of Libra Crack but is often climbed by itself. To climb the crack w/o climbing Libra there is now only a single commonly climbed option.`,
      "approach": `Right of the twin cracks above the giant ring anchors.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Gear to 2.5".`,
      "area": "620f5b8b8b8b8b8b8b8b8b8f",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106620653/pisces
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b0a", //Sport 
      "name": "BLM-6",
      "type": "SPORT",
      "grade": {
                "text" : "5.10d",
                "value" : 5.104,
              },
      "description": `The best 5.10 at Exit 32? Start up the thick flake (left of Dairy Freeze) to clip the first bolt. Follows a series of reachy, balancy moves up the great high friction face. Pretty much every move makes you think, and just when you are thinking this is hard, a jug appears that you missed by about 2 inches. Fun, long, exposed and well-protected, and a great warmup if the line for Abo and Pyschowussy is too long.`,
      "approach": `Up the hill on the left side of WWI, immediately left of Dairy Freeze. Start on the short, thick layback crack that leads to the first bolt just above it. There's a spray-painted red circle on the wall a few feet to the left of the start of this climb.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. 10 bolts (mix of glue-ins and expansion) + chains",
      "area": "620f5b8b8b8b8b8b8b8b8b8f",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105840317/blm-6
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b0b", //Sport 
      "name": "Primus",
      "type": "SPORT",
      "grade": {
                "text" : "5.11a",
                "value" : 5.111,
              },
      "description": `climb to the roof and pull the lip using an awsome heel hook, then after that the crux is just fighting the pump near the upper section of the wall. you will see a bolt a little more than halfway up the wall that has been used as a bail biner, this is where the pump really starts to get ya.`,
      "approach": `three routes to the left of the wet spot.`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. 6 draw plus 2 for the chains. rap or lower frim the chains.",
      "area": "620f5b8b8b8b8b8b8b8b8b8f",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/106006893/primus
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b0c", //TR*  rareté des areas 
      "name": "More than 24 Dandelions",
      "type": "TOPROPE",
      "grade": {
                "text" : "5.8",
                "value" : 5.08,
              },
      "description": `Start from Buzz's ledge (big ledge on right side), climb with mixed techniques up the broken corner crack to a small roof with a finger crack coming out the right side. Follow this splitter as it widens to hands and goes to the top. The rock is really grippy and the angle is fairly low, so it may make for excellent practice at finger-to-hand jamming for the new trad climber. Old trad climbers like it too.`,
      "approach": `The crack splits the top of this formation. It starts just above "Milkman" and can easily be linked with that pitch.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Nuts and cams to 3".`,
      "area": "620f5b8b8b8b8b8b8b8b8b8f",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/122603709/more-than-24-dandelions 

    { //area: 630f5b8b8b8b8b8b8b8b8b8g "630f5b8b8b8b8b8b8b8b8b83", "630f5b8b8b8b8b8b8b8b8b84", "630f5b8b8b8b8b8b8b8b8b85", "630f5b8b8b8b8b8b8b8b8b86", "630f5b8b8b8b8b8b8b8b8b87"
      "_id": "630f5b8b8b8b8b8b8b8b8b0d", //Trad 
      "name": "Sweet Surprise",
      "type": "TRAD",
      "grade": {
                "text" : "5.8",
                "value" : 5.08,
              },
      "description": `Climb the obvious crack on the right side of the formation. Start up a fun triangular flake. Above climb thin hands to a seam. Belay on the ledge above with gear.`,
      "approach": `Obvous crack on the right side of the formation.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Wires, Cams thin to medium.`,
      "area": "620f5b8b8b8b8b8b8b8b8b81",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105811067/sweet-surprise
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b0e", //Trad 
      "name": "Naked Edge",
      "type": "TRAD",
      "grade": {
                "text" : 5.9,
                "value" : 5.09,
              },
      "description": `This climb is on the south east side and is the rightmost bolted line on the face. It starts up a nice finger crack. Climb crack up and past the first two bolts where a bit of face climbing leads to a third bolt. From there, slap the right arete to the top and a 2 bolt anchor.`,
      "approach": `Southeast side of the Wedge, the rightmost route.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Three bolts, two-bolt anchor rap on top. Couple cams up to 1/2" might be desired getting up to the first bolt.`,
      "area": "620f5b8b8b8b8b8b8b8b8b81",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105802670/naked-edge 
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b0f", //Sport 
      "name": "East Face",
      "type": "SPORT",
      "grade": {
                "text" : "5.8",
                "value" : 5.08,
              },
      "description": `First find a large Boulder that makes a small cave on Southeast side of The Monk. Climb bolted face up and and to the right around corner to access the East face.`,
      "approach": `The monk is the tower that is above the headwall, you cant miss it, route is on the east side.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. 9 bolts to top. The top has several thick metal rings directly on the rock.  You can use two as a top belay anchor but they'll be one above another not side by side.  Belaying up a second can be a bit uncomfortable to get below the anchors.  Rappelling back down the route requires a 70m rope.  You can rappel off the south face with a 60m rope (90 degrees left of the way you came up).`,
      "area": "620f5b8b8b8b8b8b8b8b8b81",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105801358/east-face
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b1a", //Sport*  rareté des areas 
      "name": "Lesson in Discipline",
      "type": "SPORT",
      "grade": {
                "text" : "5.11c",
                "value" : 5.113,
              },
      "description": `Door jamb edges up a gently overhanging face until finally a rest is encountered. Climb starts same as Powder Puff, then branches off right and up face. Excellent TR if not ready for the lead (I spent many a shady late summer afternoons messing around on this).`,
      "approach": `Start same as Powder Puff. Continue to summit and rap, or downclimb from sub summit on south crack (5.3) or beer route (5th class, above 1st pitch Chug A Lug- best access for TR and normally shaded).`,
      "descent": "Use your harness kit. And, wear your security equipment and professional gears. Caution is required. Stoppers and 4 bolts.",
      "area": "620f5b8b8b8b8b8b8b8b8b81",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/105981032/lesson-in-discipline
    {
      "_id": "630f5b8b8b8b8b8b8b8b8b1b", //TR*  rareté des areas
      "name": "Sphinctre Boy",
      "type": "TOPROPE",
      "grade": {
                "text" : "5.8",
                "value" : 5.08,
              },
      "description": `This is a right arching finger crack. Start in the crack climb it to its end then continue up face and slightly left to a two bolt anchor.`,
      "approach": `After arriving at the base of the large slab scramble up and right of face to base of Girlie Man. The crack is 5 feet to the right of Girlie Man.`,
      "descent": `Use your harness kit. And, wear your security equipment and professional gears. Caution is required. A single set of Cams or stoppers up to 0.75" works well.`,
      "area": "620f5b8b8b8b8b8b8b8b8b81",
      "user": "634f5b8b8b8b8b8b8b8b8b1b",
      "createdAt": "2024-02-20T10:00:00.000Z",
      "updatedAt": "2024-02-20T10:00:00.000Z",
    }, //https://www.mountainproject.com/route/107844949/sphinctre-boy
]

  module.exports = routes;