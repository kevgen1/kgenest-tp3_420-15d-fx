
const users = [
    {
      "_id": "634f5b8b8b8b8b8b8b8b8b1a",
      "username": "user1",
      "email": "user1@example.com",
      "password": "$2b$10$XcEjpS05xnsqBKc./31Ne.DJl4m2FpBpAjZDucaurqe44QhIg2Peq",
      "createdAt": "2024-02-20T11:00:00.000Z",
      "updatedAt": "2024-02-20T11:00:00.000Z"
    },
    {
      "_id": "634f5b8b8b8b8b8b8b8b8b1b",
      "username": "user2",
      "email": "user2@example.com",
      "password": "$2b$10$XcEjpS05xnsqBKc./31Ne.DJl4m2FpBpAjZDucaurqe44QhIg2Peq",
      "createdAt": "2024-02-20T11:10:00.000Z",
      "updatedAt": "2024-02-20T11:10:00.000Z"
    },
  ]
  

module.exports = users;
